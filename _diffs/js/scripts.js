$(document).ready(function() {
 
  $("#owl-demo").owlCarousel({
    autoPlay : 3000,
    stopOnHover : true,
    navigation:false,
    paginationSpeed : 1000,
    goToFirstSpeed : 2000,
    singleItem : true,
    autoHeight : true,
    transitionStyle:"fade"
  });
    
  $("#latestAct").owlCarousel({
    autoPlay : 3000,
    stopOnHover : true,
    navigation:false,
    paginationSpeed : 1000,
    goToFirstSpeed : 2000,
    singleItem : true,
    autoHeight : true,
    transitionStyle:"fade"
  });
    
  var owl = $("#partner");
 
  owl.owlCarousel({
     
      autoPlay: 8000, //Set AutoPlay to 3 seconds
 
      items : 6,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]
    
});
    
    
  $("#testimonial").owlCarousel({
 
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      autoPlay : 6000,
      navigation:false,
      singleItem:true
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
  });
  
$('ul.nav li.dropdown').hover(function() {
	  $(this).find('.dropdown-menus').stop(true, true).delay(100).fadeIn(500);
	}, function() {
	  $(this).find('.dropdown-menus').stop(true, true).delay(100).fadeOut(500);
	});

$('#Grid').mixitup();

$(".responsive-calendar").responsiveCalendar({
	 time: '2014-02',
	 events: {
	"2014-02-05": {"number": 5, "url": "http://w3widgets.com/responsive-slider"},
	"2014-02-16": {"number": 1, "url": "http://w3widgets.com"}, 
	"2014-02-28":{"number": 1}, 
	"2014-03-12": {}}
});


 
});